# README #

Aupa Izaskun,

Te dejo aquí la lista de funciones con la que necesitaría más ayuda.
Son las operaciones básicas de matrices, así que habría que encontrar 
la manera más eficiente de hacerlas en el cluster de GPUS.

definitions.c -> 
	Kron
	sumM
	mulM
	
Con estas funciones básicas, el resto de cosas las puedo escribir sin problema.